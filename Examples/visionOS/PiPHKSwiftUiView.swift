import HaishinKit
import SRTHaishinKit
import SwiftUI

struct PiPHKSwiftUiView: UIViewRepresentable {
    var piphkView = PiPHKView(frame: .zero)

    @Binding var stream: SRTStream

    func makeUIView(context: Context) -> PiPHKView {
        piphkView.videoGravity = .resizeAspectFill
        return piphkView
    }

    func updateUIView(_ uiView: PiPHKView, context: Context) {
        piphkView.attachStream(stream)
    }
}
