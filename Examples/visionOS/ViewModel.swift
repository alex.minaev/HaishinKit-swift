import Foundation

import SRTHaishinKit

final class ViewModel: ObservableObject {
    let maxRetryCount: Int = 5

    private var srtConnection = SRTConnection()
    @Published var srtStream: SRTStream!
    private var retryCount = 0

    func config() {
        srtStream = SRTStream(connection: srtConnection)
    }

    func unregisterForPublishEvent() {
        srtStream.close()
    }

    func startPlaying() {
        logger.info(Preference.defaultInstance.uri!)
        
        srtConnection.open(URL(string: Preference.defaultInstance.uri!)!)
        srtStream.play()
    }

    func stopPlaying() {
        srtConnection.close()
    }
}
