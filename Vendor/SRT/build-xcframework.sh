#!/bin/bash

if which $(pwd)/OpenSSL >/dev/null; then
  echo ""
else
  git clone git@github.com:zulkis/OpenSSL.git

  echo "zulkis's fork does not have pre-built OpenSSL for swift for XROS. Contact maintainer."; exit -1; 
fi

if which $(pwd)/srt >/dev/null; then
  echo ""
else
  git clone git@github.com:zulkis/srt.git
#  pushd srt
#  git checkout refs/tags/v1.5.3
#  popd
fi

rm -rf Includes
mkdir Includes
cp srt/srtcore/*.h Includes

srt() {
  IOS_OPENSSL=$(pwd)/OpenSSL/$1

  mkdir -p ./build/$1/$2/$3
  pushd ./build/$1/$2/$3
  ../../../../srt/configure --cmake-prefix-path=$IOS_OPENSSL --ios-disable-bitcode=1 --ios-platform=$2 --ios-arch=$3 --cmake-toolchain-file=scripts/iOS.cmake --USE_OPENSSL_PC=off
  make
  popd
}

srt_macosx() {
  OPENSSL=$(pwd)/OpenSSL/$1

  mkdir -p ./build/$1/$2
  pushd ./build/$1/$2
  ../../../srt/configure --cmake-prefix-path=$OPENSSL --cmake-osx-architectures=$2 --USE_OPENSSL_PC=ON --ssl-include-dir=$OPENSSL/include --ssl-libraries=$OPENSSL/lib/libcrypto.a
  make
  popd
}

srt_visionos() {
  VISION_OPENSSL=$(pwd)/OpenSSL/$1

  mkdir -p ./build/$1/$2/$3
  pushd ./build/$1/$2/$3
  ../../../../srt/configure --cmake-prefix-path=$VISION_OPENSSL --visionos-platform=$2 --visionos-arch=$3 --cmake-toolchain-file=scripts/visionOS.cmake --USE_OPENSSL_PC=off
  make
  popd
}


# macOS
srt_macosx macosx arm64
libtool -static -o ./build/macosx/libsrt.a ./build/macosx/arm64/libsrt.a ./OpenSSL/macosx/lib/libcrypto.a ./OpenSSL/macosx/lib/libssl.a

# visionOS
export XROS_DEPLOYMENT_TARGET=1.0
SDKVERSION=$(xcrun --sdk xros --show-sdk-version)
srt_visionos visionos OS arm64
srt_visionos visionsimulator SIMULATOR arm64

libtool -static -o ./build/visionos/OS/libsrt.a ./build/visionos/OS/arm64/libsrt.a ./OpenSSL/visionos/lib/libcrypto.a ./OpenSSL/visionos/lib/libssl.a
libtool -static -o ./build/visionsimulator/SIMULATOR/libsrt.a ./build/visionsimulator/SIMULATOR/arm64/libsrt.a ./OpenSSL/visionsimulator/lib/libcrypto.a ./OpenSSL/visionsimulator/lib/libssl.a
unset SDKVERSION
unset XROS_DEPLOYMENT_TARGET

# iOS
export IPHONEOS_DEPLOYMENT_TARGET=12.0
SDKVERSION=$(xcrun --sdk iphoneos --show-sdk-version)
srt iphoneos OS arm64
srt iphonesimulator SIMULATOR64 arm64

libtool -static -o ./build/iphoneos/OS/libsrt.a ./build/iphoneos/OS/arm64/libsrt.a ./OpenSSL/iphoneos/lib/libcrypto.a ./OpenSSL/iphoneos/lib/libssl.a
libtool -static -o ./build/iphonesimulator/SIMULATOR64/libsrt.a ./build/iphonesimulator/SIMULATOR64/arm64/libsrt.a ./OpenSSL/iphonesimulator/lib/libcrypto.a ./OpenSSL/iphonesimulator/lib/libssl.a
unset SDKVERSION
unset IPHONEOS_DEPLOYMENT_TARGET

# make libsrt.xcframework
cp module.modulemap Includes/module.modulemap
cp ./build/macosx/arm64/version.h Includes/version.h

rm -rf libsrt.xcframework
xcodebuild -create-xcframework \
    -library ./build/visionos/OS/libsrt.a -headers Includes \
    -library ./build/visionsimulator/SIMULATOR/libsrt.a -headers Includes \
    -library ./build/iphoneos/OS/libsrt.a -headers Includes \
    -library ./build/iphonesimulator/SIMULATOR64/libsrt.a -headers Includes \
    -library ./build/macosx/libsrt.a -headers Includes \
    -output libsrt.xcframework

